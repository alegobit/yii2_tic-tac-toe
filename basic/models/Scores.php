<?php

namespace app\models;

use yii\db\ActiveRecord;

class Scores extends ActiveRecord
{    
    public function getScore($uid=array()){
      /*
      $uid = [ 
                0 => 1,
                1 => 3,
                2 => 5
              ];
      */
      $sql = '';        
      for($i=0;$i<count($uid);$i++){ 
        $sql .= 'SELECT u.id, u.name, SUM(s.score) score FROM scores AS s 
                JOIN user AS u ON u.id = s.user_id 
                WHERE s.user_id = '.$uid[$i]; 
        if($i==count($uid)-1){
          $sql .= ' ORDER BY score DESC';
        } else {      
          $sql .= ' UNION ';
        }        
      }
        return $this->findBySql($sql)->all();
    }
}