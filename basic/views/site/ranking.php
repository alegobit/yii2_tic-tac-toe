<?php
use yii\helpers\Html;

$scores = new app\models\Scores;
/* @var $this yii\web\View */
$this->title = 'Ranking';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
      <div class="col-md-6">
        <p>
            Your Ranks between Friends
        </p>        
        <div class="table-responsive">
          <table class="table table-striped dt-right compact">
          <thead>
            <tr>
              <th>#</th>
              <th>name</th>
              <th>score</th>
            </tr>
          </thead>
          <tbody>
          <?php            
            $i=0;
            $uid=array();
            foreach (Yii::$app->user->identity->getMeNFriends() as $mnf) {
              $uid[$i] = $mnf['id'];
              $i++;
            }
            $j=0;
            foreach ($scores->getScore($uid) as $rank) {
          ?>  
            <tr class="<?=($rank->id==Yii::$app->user->identity->id)?'info text-info':''?>">    
              <td><?=$j+1?></td>
              <td><?=Yii::$app->user->identity->findIdentity($rank->id)->name?></td>
              <td><?=$rank->score?></td>
            </tr> 
          <?php
              $j++;
            }
          ?> 
          </tbody>  
          </table>                     
        </div>                    
      </div>
    </div>

</div>
