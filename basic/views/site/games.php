<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'My Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-games">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
      <div class="col-md-6">
      <p>
          Select random opponent to start a game
      </p>
      <?php
        foreach (Yii::$app->user->identity->getRandomUserName() as $opp) {
      ?>              
      <p>
        <a class="startgame btn btn-lg btn-success" href="#" 
            data-uid="<?=@Yii::$app->user->identity->id?>"
            data-oid="<?=$opp['id']?>"
            data-unm="<?=@Yii::$app->user->identity->name?>"
            data-onm="<?=$opp['name']?>">
          <?=$opp['name']?>
        </a>
      </p>
      <?php
        }
      ?>                         
      </div>
      <div class="col-md-6">
              <div id="versus" class="text-center"></div> 
              <div id="board-outer" class="center">
                  <table id="board">
                      <tr class="row">
                          <td class="td" id="a1"></td>
                          <td class="td" id="a2"></td>
                          <td class="td" id="a3"></td>
                      </tr>
                      <tr class="row">
                          <td class="td" id="b1"></td>
                          <td class="td" id="b2"></td>
                          <td class="td" id="b3"></td>
                      </tr>
                      <tr class="row">
                          <td class="td" id="c1"></td>
                          <td class="td" id="c2"></td>
                          <td class="td" id="c3"></td>
                      </tr>
                  </table>
              </div>
              <br>
              <div class="center button">
                  <button id="restart">New game</button>
              </div>         
      </div>
    </div>
    <div id="post" data-url="<?=Url::toRoute('/site/post')?>"></div>
</div>