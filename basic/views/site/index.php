<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Online Tic Tac Toe';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Play Now!</h1>

        <p class="lead">Play Online Tic Tac Toe with Your Friends.</p>

        <p><a href="<?=Url::toRoute('/site/login')?>"><img class="img-responsive" style="margin:0 auto;" src="img/t3.jpg"></a></p>
        <p><a class="btn btn-lg btn-success" href="<?=Url::toRoute('/site/login')?>">Login to Play</a></p>
    </div>
</div>
