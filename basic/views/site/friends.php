<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'My Friends';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
      <div class="col-md-6">
      <p>
          Choose your friend to play a game with
      </p>
      <?php
        foreach (Yii::$app->user->identity->getFriendsName() as $friends) {
      ?>              
      <p>
        <a class="startgame btn btn-lg btn-success" href="#" 
            data-uid="<?=@Yii::$app->user->identity->id?>"
            data-oid="<?=$friends['id']?>"
            data-unm="<?=@Yii::$app->user->identity->name?>"
            data-onm="<?=$friends['name']?>">
          <?=$friends['name']?>
        </a>
      </p>
      <?php
        }
      ?>                         
      </div>
      <div class="col-md-6">    
              <div id="versus" class="text-center"></div> 
              <div id="board-outer" class="center">
                  <table id="board">
                      <tr class="row">
                          <td class="td" id="a1"></td>
                          <td class="td" id="a2"></td>
                          <td class="td" id="a3"></td>
                      </tr>
                      <tr class="row">
                          <td class="td" id="b1"></td>
                          <td class="td" id="b2"></td>
                          <td class="td" id="b3"></td>
                      </tr>
                      <tr class="row">
                          <td class="td" id="c1"></td>
                          <td class="td" id="c2"></td>
                          <td class="td" id="c3"></td>
                      </tr>
                  </table>
              </div>
              <br>
              <div class="center button">
                  <button id="restart">New game</button>
              </div>         
      </div>
    </div>

</div>
