$(document).ready(function(){  
    $('.startgame').click(function(){
        var postUrl = $('#post').data('url');
        var unm = $(this).data('unm');
        var onm = $(this).data('onm');
        $.ajax({
            type: "POST",
            url: postUrl,
            data: { 
              uid: $(this).data('uid'), 
              oid: $(this).data('oid')
            },
            cache: false,
            success: function(result){
              /* 
              if(result.trim()!='OK'){    
                alert(result); 
              } else {    
        				$this.parents('tr').fadeOut(function(){
        					$this.remove(); //remove row when animation is finished
        				});
                var page = window.location.href;
                window.location.assign(page);     
              } 
              */   
              $('#versus').html('<h2>'+unm+' vs '+onm+'</h2>');
            }
          }); 
    });
});